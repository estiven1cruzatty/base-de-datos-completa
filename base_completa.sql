

/*==============================================================*/
/* Table: ASESORES                                              */
/*==============================================================*/
create table ASESORES (
   ID_ASESORES          INT              not null,
   CEDULA_ASESOR        INT                 null,
   NOMBRE_ASESOR        VARCHAR(40)          null,
   APELLIDO_ASESOR      VARCHAR(40)          null,
   constraint PK_ASESORES primary key (ID_ASESORES)
);


/*==============================================================*/
/* Table: CLIENTES                                              */
/*==============================================================*/
create table CLIENTES (
   ID_CLIENTE           INT               not null,
   ID_CONTRATO          INT                 not null,
   ID_ASESORES          INT                 not null,
   NOMBRE_CLIENTE       VARCHAR(40)          null,
   APELLIDO_CLIENTE     VARCHAR(40)          null,
   TIPO_SANGRE          VARCHAR(20)          null,
   FECHA_NACIMIENTO     DATE                 null,
   FECHA_INGRESO        DATE                 null,
   constraint PK_CLIENTES primary key (ID_CLIENTE)
);


/*==============================================================*/
/* Table: CONSULTAS_MEDICAS                                     */
/*==============================================================*/
create table CONSULTAS_MEDICAS (
   ID_CONSULTA          INT                not null,
   ID_CLIENTE           INT                not null,
   CONSULTA_MEDICA      VARCHAR(100)         null,
   RECETAS_MEDICAS      VARCHAR(100)         null,
   COSTO_CONSULTA       MONEY                null,
   COSTO_RECETA         MONEY                null,
   FECHA_CONSULTA       DATE                 null,
   constraint PK_CONSULTAS_MEDICAS primary key (ID_CONSULTA)
);

/*==============================================================*/
/* Table: CONTRATO_SEGURO                                       */
/*==============================================================*/
create table CONTRATO_SEGURO (
   ID_CONTRATO          SERIAL               not null,
   TIPO_CONTRATO        VARCHAR(40)          null,
   PERSONAS_ASEGURADAS  VARCHAR(50)          null,
   FECHA_NACIMIENTO     DATE                 null,
   constraint PK_CONTRATO_SEGURO primary key (ID_CONTRATO)
);

/*==============================================================*/
/* Table: COTIZACION_SEGURO                                     */
/*==============================================================*/
create table COTIZACION_SEGURO (
   ID_COTIZACION        INT               not null,
   ID_SEGURO            INT                 not null,
   TIPO_COTIZACION      VARCHAR(40)          null,
   DESCRIPCION_COTITIZACION VARCHAR(80)          null,
   VALOR_               MONEY                null,
   constraint PK_COTIZACION_SEGURO primary key (ID_COTIZACION)
);

/*==============================================================*/
/* Table: CUENTA_BANCARIA                                       */
/*==============================================================*/
create table CUENTA_BANCARIA (
   ID_CUENTA            INT                 not null,
   ID_CLIENTE           INT                 null,
   SALDO_CUENTA         NUMERIC              null,
   constraint PK_CUENTA_BANCARIA primary key (ID_CUENTA)
);

/*==============================================================*/
/* Table: DETALLE_FACTURA                                       */
/*==============================================================*/
create table DETALLE_FACTURA (
   ID_DETALLE           int                not null,
   ID_FACTURA           INT                not null,
   VALOR_CONSUMIDO      MONEY                null,
   VALOR_CUBIERTO       MONEY                null,
   VALOR_TOTAL          MONEY                null,
   constraint PK_DETALLE_FACTURA primary key (ID_DETALLE)
);


/*==============================================================*/
/* Table: DEUDAS_ASEGURADORA                                    */
/*==============================================================*/
create table DEUDAS_ASEGURADORA (
   ID_DEUDA             INT                 not null,
   ID_CLIENTE           INT                 null,
   ID_CUENTA            INT                 null,
   TIPO_DEUDA           VARCHAR(40)          null,
   VALOR_DEUDA          NUMERIC              null,
   constraint PK_DEUDAS_ASEGURADORA primary key (ID_DEUDA)
);


/*==============================================================*/
/* Table: DOCTOR                                                */
/*==============================================================*/
create table DOCTOR (
   ID_ESPECIALIDAD_     INT              not null,
   TIPO_ESPECIALIDAD    VARCHAR(40)          null,
   NOMBRE_DOCTOR        VARCHAR(40)          null,
   APELLIDO_DOCTOR      VARCHAR(40)          null,
   CEDULA_DOCTOR        INT4                 null,
   constraint PK_DOCTOR primary key (ID_ESPECIALIDAD_)
);

/*==============================================================*/
/* Table: FACTURA_SEGURO                                        */
/*==============================================================*/
create table FACTURA_SEGURO (
   ID_FACTURA           INT                not null,
   ID_CLIENTE           INT               not null,
   FECHA_FACTURA        DATE                 null,
   constraint PK_FACTURA_SEGURO primary key (ID_FACTURA)
);

/*==============================================================*/
/* Table: INCREMENTO_ANUAL                                      */
/*==============================================================*/
create table INCREMENTO_ANUAL (
   ID_INCREMENTO        INT               not null,
   ID_CONTRATO          INT                 not null,
   INCREMENTO_ANUAL     MONEY                null,
   constraint PK_INCREMENTO_ANUAL primary key (ID_INCREMENTO)
);


/*==============================================================*/
/* Table: PATOLOGIAS                                            */
/*==============================================================*/
create table PATOLOGIAS (
   ID_PATOLOGIA         INT                not null,
   ID_CLIENTE           INT                not null,
   TIPO_PATOLOGIA       VARCHAR(40)          null,
   DESCRIPCION_PATOLOGIA VARCHAR(150)         null,
   constraint PK_PATOLOGIAS primary key (ID_PATOLOGIA)
);

/*==============================================================*/
/* Table: PLANES_SALUD                                         */
/*==============================================================*/
create table PLANES__SALUD (
   ID_SEGURO            INT                 not null,
   ID_CLIENTE           INT                 not null,
   PLANES_SEGURO        VARCHAR(40)          null,
   ESTADO_SEGURO        VARCHAR(40)          null,
   PLAN_FECHA           DATE                 null,
   constraint PK_PLANES__SALUD primary key (ID_SEGURO)
);

/*==============================================================*/
/* Table: SERVICIOS                                             */
/*==============================================================*/
create table SERVICIOS (
   ID_SERVICIO          INT                 not null,
   ID_CLIENTE           INT                 not null,
   TIPO_SERVICIO        VARCHAR(40)          null,
   constraint PK_SERVICIOS primary key (ID_SERVICIO)
);

/*==============================================================*/
/* Table: TRATAMIENTO                                           */
/*==============================================================*/
create table TRATAMIENTO (
   ID_REGISTRO          INT                 not null,
   ID_ESPECIALIDAD_     INT                 not null,
   ID_CLIENTE           INT                 not null,
   FECHA_REGISTRO       DATE                 null,
   DESCRIPCION_ENFERMEDAD VARCHAR(60)          null,
   TIPODE_SANGRE        VARCHAR(20)          null,
   constraint PK_TRATAMIENTO primary key (ID_REGISTRO)
);


alter table CLIENTES
   add constraint FK_CLIENTES_RELATIONS_CONTRATO foreign key (ID_CONTRATO)
      references CONTRATO_SEGURO (ID_CONTRATO)
      

alter table CLIENTES
   add constraint FK_CLIENTES_RELATIONS_ASESORES foreign key (ID_ASESORES)
      references ASESORES (ID_ASESORES)
      

alter table CONSULTAS_MEDICAS
   add constraint FK_CONSULTA_RELATIONS_CLIENTES foreign key (ID_CLIENTE)
      references CLIENTES (ID_CLIENTE)
      

alter table COTIZACION_SEGURO
   add constraint FK_COTIZACI_RELATIONS_PLANES__ foreign key (ID_SEGURO)
      references PLANES__SALUD (ID_SEGURO)
      

alter table CUENTA_BANCARIA
   add constraint FK_CUENTA_B_RELATIONS_CLIENTES foreign key (ID_CLIENTE)
      references CLIENTES (ID_CLIENTE)

alter table DETALLE_FACTURA
   add constraint FK_DETALLE__GENERA_FACTURA_ foreign key (ID_FACTURA)
      references FACTURA_SEGURO (ID_FACTURA)

alter table DEUDAS_ASEGURADORA
   add constraint FK_DEUDAS_A_RELATIONS_CLIENTES foreign key (ID_CLIENTE)
      references CLIENTES (ID_CLIENTE)
  
alter table DEUDAS_ASEGURADORA
   add constraint FK_DEUDAS_A_RELATIONS_CUENTA_B foreign key (ID_CUENTA)
      references CUENTA_BANCARIA (ID_CUENTA)


alter table FACTURA_SEGURO
   add constraint FK_FACTURA__RELATIONS_CLIENTES foreign key (ID_CLIENTE)
      references CLIENTES (ID_CLIENTE)

alter table INCREMENTO_ANUAL
   add constraint FK_INCREMEN_RELATIONS_CONTRATO foreign key (ID_CONTRATO)
      references CONTRATO_SEGURO (ID_CONTRATO)

alter table PATOLOGIAS
   add constraint FK_PATOLOGI_RELATIONS_CLIENTES foreign key (ID_CLIENTE)
      references CLIENTES (ID_CLIENTE)

alter table PLANES__SALUD
   add constraint FK_PLANES___RELATIONS_CLIENTES foreign key (ID_CLIENTE)
      references CLIENTES (ID_CLIENTE)

alter table SERVICIOS
   add constraint FK_SERVICIO_PODRIA_CLIENTES foreign key (ID_CLIENTE)
      references CLIENTES (ID_CLIENTE)

alter table TRATAMIENTO
   add constraint FK_TRATAMIE_RELATIONS_CLIENTES foreign key (ID_CLIENTE)
      references CLIENTES (ID_CLIENTE)

alter table TRATAMIENTO
   add constraint FK_TRATAMIE_RELATIONS_DOCTOR foreign key (ID_ESPECIALIDAD_)
      references DOCTOR (ID_ESPECIALIDAD_)

ALTER TABLE CUENTA_BANCARIA ADD CHECK (SALDO_CUENTA>=0)
ALTER TABLE DEUDAS_ASEGURADORA ADD CHECK (VALOR_DEUDA>=0)
